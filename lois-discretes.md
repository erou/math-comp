## Lois discrètes

### <i class="fa fa-file-pdf-o" aria-hidden="true"></i> Fichiers de cours

+ [Cours](https://erou.forge.apps.education.fr/math-comp/pdfs/cours-lois-discretes.pdf)
+ [Exercices](https://erou.forge.apps.education.fr/math-comp/pdfs/exos-lois-discretes.pdf)

---

### <i class="fa fa-code" aria-hidden="true"></i> Algorithmes

À venir.

---

### <i class="fa fa-youtube-play" aria-hidden="true"></i> L'instant Monka

Une *playlist* d'Yvan Monka, à destination des élèves de mathématiques
complémentaires, est disponible en ligne.

<div style="text-align:center">
<iframe width="560" height="315" src="https://www.youtube.com/embed/videoseries?si=_ssn2m1Nu9-o9Iax&amp;list=PLVUDmbpupCaqbdJBmiypbs92T26fxxtvo" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>
</div>

---

### <i class="fa fa-rocket" aria-hidden="true"></i> Pour aller plus loin

À venir.
