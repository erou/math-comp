## Limites de suites

### <i class="fa fa-file-pdf-o" aria-hidden="true"></i> Fichiers de cours

+ [Cours](https://erou.forge.apps.education.fr/math-comp/pdfs/cours-limites-suites.pdf)
+ [Exercices](https://erou.forge.apps.education.fr/math-comp/pdfs/exos-limites-suites.pdf)

---

### <i class="fa fa-code" aria-hidden="true"></i> Algorithmes

Les **algorithmes de seuil** reviennent souvent dans les épreuves du baccalauréat,
car ils permettent de modéliser des situations concrètes. Ici, un exemple issu
[d'un sujet de bac de 2024](https://www.education.gouv.fr/media/195731/download)
avec la suite $$(u_n)$$ définie par $$u_0 = 0,7$$
et par $$u_{n+1} = 0,92u_n+0,3$$. On cherche à savoir au bout de combien
d'étapes la valeur de la suite dépassera la valeur $$3$$.

<iframe src="https://trinket.io/embed/python/ff0be00cbb" width="100%" height="300" frameborder="0" marginwidth="0" marginheight="0" allowfullscreen></iframe>

---

### <i class="fa fa-youtube-play" aria-hidden="true"></i> L'instant Monka

Une *playlist* d'Yvan Monka, très complète, permet de revenir sur l'ensemble des
notions liées aux suites.

<div style="text-align:center">
<iframe width="560" height="315" src="https://www.youtube.com/embed/videoseries?si=wKHXS_9ZI2N3YI3O&amp;list=PLVUDmbpupCarZdaGUMO7DV35pi1I8zIJZ" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>
</div>

---

### <i class="fa fa-rocket" aria-hidden="true"></i> Pour aller plus loin

La prochaine étape est d'étudier les
[séries](https://fr.wikipedia.org/wiki/S%C3%A9rie_(math%C3%A9matiques%29),
c'est-à-dire les suites qui sont elles-mêmes des sommes de termes de suites,
par exemple

<div style="text-align:center">
$${\displaystyle S_n = \sum_{k=0}^{n}\frac{1}{2^k} =
1+\frac{1}{2}+\frac{1}{4}+\cdots+\frac{1}{2^n}.}$$
</div>

On s'intéresse alors à la limite de ces séries, on parle de **série divergente**
si la suite $$(S_n)$$ diverge et de **série convergente** si la suite $$(S_n)$$
converge. On note alors la limite comme une somme infinie

<div style="text-align:center">
$${\displaystyle \sum_{k=0}^{+\infty}\frac{1}{2^k} = 2.}$$
</div>
