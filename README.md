<div style="text-align:center">
<h2> Option mathématiques complémentaires en Terminale</h2>
<p> Le site de votre cours d'option « Mathématiques complémentaires ». </p>
</div>

---

## <i class="fa fa-cogs" aria-hidden="true"></i> Mode d'emploi

<p>
L'icone
<i class="fa fa-align-justify"></i>
vous permet d'afficher ou de masquer le menu de navigation. Dans ce menu, vous
trouverez des ressources liées aux cours de mathématiques en Terminale, ou encore 
liées à la méthodologie.
</p>
<p>
Les flèches directionnelles
<i class="fa fa-angle-left"></i>
et
<i class="fa fa-angle-right"></i>
permettent de naviguer sur le site. Sur ordinateur, vous pouvez également
utiliser votre clavier.
</p>
<p>
L'icone
<i class="fa fa-font"></i>
vous permet de changer la police d'écriture, sa taille, ou le thème du site.
</p>
<p>
Enfin, la barre de recherche vous permet de taper un mot-clé qui sera cherché dans
l'ensemble du site.
</p>

## <i class="fa fa-envelope" aria-hidden="true"></i> Me contacter

Pour me contacter, le plus simple est de passer par la messagerie interne
du lycée. Vous pouvez également m'envoyer un mail directement sur mon adresse
AEFE : edouard.rousseau@aefe.fr.
