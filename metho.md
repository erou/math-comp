## <i class="fa fa-pencil" aria-hidden="true"></i> Méthodologie

### <i class="fa fa-hourglass-end" aria-hidden="true"></i> Espacer son travail

Pour mieux apprendre, il est préférable de travailler régulièrement tout
au long de l'année, plutôt que de faire de grands efforts juste avant les
contrôles. De la même manière, il est préférable de travailler trois fois une
heure qu'une seule fois trois heures. **Cela permet de mieux ancrer vos
connaissances dans votre cerveau.**

Dans l'idéal, vous prenez environ dix minutes **le jour même** pour relire et
mémoriser le contenu du cours.

### <i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i> Être dans l'action

Lorsque vous voulez réviser votre cours, ne le relisez pas passivement, à
part peut-être la première fois évidemment. Il est plus utile de vérifier que
vous l'avez retenu en vous posant des questions :
+ est-ce que je sais réécrire cette formule ?
+ Est-ce que j'ai compris à quoi cette formule pouvait servir, ou dans quel cas
  je pouvais l'utiliser ?
+ Est-ce que je sais réécrire la définition sans la regarder ? 
+ Quel était le point central de ce chapitre ?

Idem pour les exercices faits en classe, ne relisez pas simplement la correction
en vérifiant que vous la comprenez. Il faut vraiment vérifier que vous savez
refaire les exercices sans regarder la correction, à part à la fin pour
constater que vous n'avez pas fait d'erreur. De cette manière, vous vérifiez que
vous êtes bien **capable de réutiliser les outils du cours**.

### <i class="fa fa-compass" aria-hidden="true"></i> Ressources 

Une vidéo de David Louapre (Science étonnante) sur la question de
l'apprentissage, qui reprend les conseils ci-dessus et va plus loin.

 <div style="text-align:center">
<iframe width="560" height="315" src="https://www.youtube.com/embed/RVB3PBPxMWg?si=WhgFgd2UcM_uExhG" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
</div>
