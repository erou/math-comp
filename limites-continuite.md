## Limites et continuité

### <i class="fa fa-file-pdf-o" aria-hidden="true"></i> Fichiers de cours

+ [Cours](https://erou.forge.apps.education.fr/math-comp/pdfs/cours-limites-continuite.pdf)
+ [Exercices](https://erou.forge.apps.education.fr/math-comp/pdfs/exos-limites-continuite.pdf)

---

### <i class="fa fa-youtube-play" aria-hidden="true"></i> L'instant Monka

Il faut ici plusieurs *playlists* car il y a en fait plusieurs notions dans ce
chapitre : les limites d'une part et la continuité d'autre part, qui ne sont pas
toujours présentées ensemble.

*À propos des limites :*

<div style="text-align:center">
<iframe width="560" height="315" src="https://www.youtube.com/embed/videoseries?si=vSx9Getwna9YjYJd&amp;list=PLVUDmbpupCarS4Qp45vTwsEGYMOJtgBxE" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>
</div>

*À propos de la continuité :*

<div style="text-align:center">
<iframe width="560" height="315" src="https://www.youtube.com/embed/videoseries?si=9EF-JFr4AmwqSVE1&amp;list=PLVUDmbpupCaru94MHSdOLsjrM0Ppe2kfv" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>
</div>

---

### <i class="fa fa-rocket" aria-hidden="true"></i> Pour aller plus loin

On a vu que les fonctions dérivables étaient forcément continues. La réciproque
est fausse, même si toutes les fonctions rencontrées dans ce chapitre étaient à
la fois continues et dérivables. Il existe même des fonctions continues en tout
point mais dérivables en aucun point : la figure ci-dessous montre la
construction d'une telle fonction. Elle est due aux mathématiciens Karl
Weierstrass et Leopold Kronecker. Voir [l'article
Wikipédia](https://fr.wikipedia.org/wiki/Fonction_de_Weierstrass) pour plus de
renseignements.

<div style="text-align:center">
<img
src="https://upload.wikimedia.org/wikipedia/commons/2/29/Weierstrass_Animation.gif" width="70%" alt="La fonction de Weiestrass.">
</div>
